const express = require('express')
const Pool = require('pg').Pool
const app = express()

const PORT = process.env.PORT || 6969

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};

app.use(express.json())
app.use(allowCrossDomain)

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false
  }
})

app.get('/api/getScores', async (req, res) => {
  try {
    const client = await pool.connect()
    const result = await client.query(`SELECT (name, score) FROM scores ORDER BY score DESC;`)
    let results = []
    result.rows.map(rowObj => {
      const rowVals = rowObj.row.substring(1, rowObj.row.length - 1).split(',')
      results.push({name: rowVals[0], score: rowVals[1]})
    })
    res.json(results)
    client.release()
  } catch (err) {
    console.error(err)
    res.json({error: err})
  }
})

app.get('/api/getLeaderboard', async (req, res) => {
  try {
    const client = await pool.connect()
    const result = await client.query(`SELECT (name, score) FROM scores ORDER BY score DESC FETCH FIRST 10 ROWS ONLY;`)
    let results = []
    result.rows.map(rowObj => {
      const rowVals = rowObj.row.substring(1, rowObj.row.length - 1).split(',')
      results.push({name: rowVals[0], score: rowVals[1]})
    })
    res.json(results)
    client.release()
  } catch (err) {
    console.error(err)
    res.json({error: err})
  }
})

app.post('/api/addScore', async (req, res) => {
  const {name, score} = req.body
  try {
    const client = await pool.connect()
    const result = await client.query(`INSERT INTO scores (name, score) VALUES('${name}', ${score});`)
    res.json({
      success: true,
      message: 'Inserted Score!'
    })
    client.release()
  } catch (err) {
    console.error(err)
    res.json({error: err})
  }
})

const server = app.listen(PORT, function () {
  const host = server.address().address
  const port = server.address().port
  console.log(`App Listening on port ${port}`)
})